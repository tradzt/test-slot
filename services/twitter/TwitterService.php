<?php

namespace slotegrator\services\twitter;

use Yii;
use yii\authclient\clients\Twitter;
use yii\authclient\OAuthToken;
use yii\db\ActiveRecord;

class TwitterService
{

    private $twitter;

    public function __construct()
    {
        // create OAuthToken
        $token = new OAuthToken([
            'token' => Yii::$app->params['twitterAccessToken'],
            'tokenSecret' => Yii::$app->params['twitterAccessTokenSecret']
        ]);
        // start a Twitter Client and configure access token with recently created token
        $this->twitter = new Twitter([
            'accessToken' => $token,
            'consumerKey' => Yii::$app->params['twitterApiKey'],
            'consumerSecret' => Yii::$app->params['twitterApiSecret']
        ]);

    }

    /**
     * Get user tweets from twitter API
     * @param $users
     * @return array
     */
    public function getFeed($users): array
    {

        $returnData = [];

        foreach ($users as $user) {

            $response = $this->twitter->api(
                'statuses/user_timeline.json',
                'GET',
                [
                    'screen_name' => $user['name'],
                    'since_id' => !empty($user['latest_tweet']) ? $user['latest_tweet'] : null
                ]
            );

            $returnData = array_merge(
                $returnData,
                $this->formatResponse($response)
            );

            if (isset($response[0]['id']) && $user instanceof ActiveRecord) {
                $user->latest_tweet = $response[0]['id'];
                $user->update();
            }

        }

        return $returnData;
    }


    /**
     * Format response
     * @param $data
     * @return array
     */
    private function formatResponse(array $data): array
    {
        $format_response = [];

        foreach ($data as $item) {
            $format_response[] = [
                'user' => isset($item['user']['screen_name']) ? $item['user']['screen_name'] : '',
                'tweet' => isset($item['text']) ? $item['text'] : '',
                'hashtag' => isset($item['entities']['hashtags']) ? $this->formatHashTags($item['entities']['hashtags']) : []
            ];
        }

        return $format_response;
    }

    /**
     * Format hashtag array
     * @param $hash_array
     * @return array
     */
    private function formatHashTags(array $hash_array): array
    {
        $hashtags = [];

        foreach ($hash_array as $item) {
            if (isset($item['text'])) {
                $hashtags[] = $item['text'];
            }
        }

        return $hashtags;
    }

}