<?php

namespace slotegrator\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%users%}}";
    }

    public function rules()
    {
        return [
            ['name', 'string', 'length' => [1,255]],
            ['name', 'unique', 'message' => 'User exist'],
        ];
    }

}