<?php


namespace slotegrator\controllers;

use slotegrator\models\User;
use slotegrator\services\twitter\TwitterService;
use Yii;
use yii\base\DynamicModel;

class ApiController extends BaseController
{

    /**
     *
     * @param string $id - Random 32-char string used as unique identifier of a request
     * @param string $user - Twitter username of an user that should be added to my list
     * @param string $secret - Secret parameter to be used as security layer
     * @return yii\web\Response
     */
    public function actionAdd($id = '', $user = '', $secret = '')
    {
        $validate = $this->validate($id, $secret, $user);

        if ($validate !== true) {
            return $this->errorResponse(array_shift($validate));
        }

        $userModel = new User();
        $userModel->name = $user;
        if (!$userModel->save()) {
            return $this->errorResponse(array_shift($userModel->firstErrors));
        }
    }


    /**
     * @param string $id
     * @param string $secret
     * @return void|Yii\web\Response
     */
    public function actionFeed($id = '', $secret = '')
    {
        $validate = $this->validate($id, $secret);
        if ($validate !== true) {
            return $this->errorResponse(array_shift($validate));
        }

        $users = User::find()->all();
        if (count($users) == 0) {
            return; //If user list is empty, response should be blank.
        }

        $returnData = (new TwitterService())->getFeed($users);


        return $this->dataResponse($returnData);
    }

    /**
     * @param string $id
     * @param string $user
     * @param string $secret
     * @return \yii\web\Response
     */
    public function actionRemove($id = '', $user = '', $secret = '')
    {
        $validate = $this->validate($id, $secret, $user);
        if ($validate !== true) {
            return $this->errorResponse(array_shift($validate));
        }

        $userModel = User::find()->where(['name' => $user])->one();
        if (!$userModel) {
            return $this->errorResponse('User not exist');
        }
        $userModel->delete();
    }

    /**
     * @param string $id
     * @param string $user
     * @return string
     */
    public function getSecretString(string $id, string $user)
    {
        return sha1($id . $user);
    }

    /**
     * @param $id
     * @param $secret
     * @param bool $user
     * @return array|bool
     */
    public function validate($id, $secret, $user = false)
    {
        if ($user === false) {
            $requirements = [
                [['id', 'secret'], 'required', 'message' => 'missing parameter'],
                [['secret'], 'string', 'length' => [1, 255], 'message' => 'missing parameter'],
                ['id', 'string', 'length' => 32, 'message' => 'missing parameter'],
                ['secret', 'compare', 'compareValue' => $this->getSecretString($id, ''), 'message' => 'access denied']
            ];
        } else {
            $requirements = [
                [['id', 'secret', 'user'], 'required', 'message' => 'missing parameter'],
                [['secret', 'user'], 'string', 'length' => [1, 255], 'message' => 'missing parameter'],
                ['id', 'string', 'length' => 32, 'message' => 'missing parameter'],
                [
                    'secret',
                    'compare',
                    'compareValue' => $this->getSecretString($id, $user),
                    'message' => 'access denied'
                ]
            ];
        }


        $model = DynamicModel::validateData(compact('id', 'secret', 'user'), $requirements);

        if ($model->hasErrors()) {
            return $model->getFirstErrors();
        }

        return true;
    }
}