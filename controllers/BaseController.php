<?php

namespace slotegrator\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{

    /**
     * Return response with error
     * @param $error
     * @return \yii\console\Response|Response
     */
    public function errorResponse(string $error)
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'error' => $error
        ];
        return $response;
    }


    /**
     * @param $data
     * @return \yii\console\Response|Response
     */
    public function dataResponse($data)
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = $data;
        return $response;
    }
}